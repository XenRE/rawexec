/*
 * rawexec: boot raw and multiboot modules via Linux kexec API
 *
 * Copyright (C) 2021  Xen (https://gitlab.com/XenRE)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 2 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include "kexec-syscall.h"
#include "multiboot.h"

#ifndef PAGE_SIZE
#define PAGE_SIZE 4096
#endif

uint8_t MbExecCode_x32[] = {
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xE8,0x00,0x00,0x00,0x00,0x5A,0x81,0xEA,
0x0D,0x00,0x00,0x00,0x8B,0x5A,0x04,0x8B,0x12,0xB8,0x02,0xB0,0xAD,0x2B,0xFF,0xE2};

#define MbExec_Exec 0
#define MbExec_Info 1
#define MbExec_Entry 2

typedef struct ModuleDescr_s ModuleDescr_t;
typedef struct multiboot_header multiboot_header_t;

struct ModuleDescr_s {
	uint8_t * Module;
	size_t ModuleSize;
	const char * CmdLine;
	size_t CmdLineSize;
	ModuleDescr_t * Next;
};

void * LoadFile(const char * FileName, size_t* Size)
{
	FILE * file;
	size_t fsize;
	void * buf = NULL;
	
	file = fopen(FileName, "rb");
	if (file == NULL) {
		fprintf(stderr, "failed to open file %s\n", FileName);
		return NULL;
	}

	fseek(file, 0, SEEK_END);
	fsize = ftell(file);
	if (fsize >= 1){
		fseek(file, 0, SEEK_SET);
		
		buf = malloc(fsize);
		if (buf != NULL) {
			if (fread(buf, 1, fsize, file) == fsize) {
				*Size = fsize;
			} else {
				fprintf(stderr, "failed to read file %s\n", FileName);
				free(buf);
				buf = NULL;
			}
		} else {
			fprintf(stderr, "failed to allocate %d bytes for file %s\n", fsize, FileName);
		}
	} else {
		fprintf(stderr, "file %s is empty or error\n", FileName);
		
	}
	fclose(file);
	return buf;
}

ModuleDescr_t * LoadModule(const char * FileName, const char * CmdLine)
{
	size_t Size;
	void * Module;
	ModuleDescr_t * Descr;

	Module = LoadFile(FileName, &Size);
	if (Module == NULL) return NULL;
	Descr = malloc(sizeof(ModuleDescr_t));
	if (Descr == NULL) {
		free(Module);
		return NULL;
	}
	Descr->Module = Module;
	Descr->ModuleSize = Size;
	Descr->CmdLine = CmdLine;
	if (CmdLine != NULL) {
		Descr->CmdLineSize = strlen(CmdLine) + 1;
	}
	Descr->Next = NULL;
	return Descr;
}

void FreeModules(ModuleDescr_t * Module)
{
	ModuleDescr_t * CurrMod = Module;
	ModuleDescr_t * NextMod;

	while (CurrMod != NULL) {
		NextMod = CurrMod->Next;
		if (CurrMod->Module) free(CurrMod->Module);
		free(CurrMod);
		CurrMod = NextMod;
	}
}

ModuleDescr_t * LoadModules(int count, char *argv[])
{
	ModuleDescr_t * FirstMod = NULL;
	ModuleDescr_t * CurrMod;
	ModuleDescr_t * LastMod;
	int i;

	for (i = 0; i < count; i++) {
		CurrMod = LoadModule(argv[i * 2], argv[i * 2 + 1]);
		if (CurrMod == NULL) {
			FreeModules(FirstMod);
			return NULL;
		}
		if (FirstMod == NULL) {
			FirstMod = CurrMod;
		} else {
			LastMod->Next = CurrMod;
		}
		LastMod = CurrMod;
	}
	return FirstMod;
}

multiboot_header_t * MultibootFindHeader(uint8_t * buf, uint32_t buf_size)
{
	uint32_t offs;
	uint32_t max_offs = buf_size - sizeof(multiboot_header_t);
	multiboot_header_t * hdr;

	if (buf_size < sizeof(multiboot_header_t)) return NULL;
	if (max_offs > MULTIBOOT_SEARCH) max_offs = MULTIBOOT_SEARCH;
	for (offs = 0; offs < max_offs; offs +=MULTIBOOT_HEADER_ALIGN)
	{
		hdr = (multiboot_header_t *)(buf + offs);
		if (hdr->magic == MULTIBOOT_HEADER_MAGIC)
		{
			if ((hdr->magic + hdr->flags + hdr->checksum) == 0)
			{
				return hdr;
			}
		}
	}
	return NULL;
}

void SetSegmentMemsz(struct kexec_segment * Segm)
{
	Segm->memsz = (Segm->bufsz + (PAGE_SIZE - 1)) & ~(PAGE_SIZE - 1);
}

struct kexec_segment * CreateRawKexecSegments(ModuleDescr_t * Modules, int ModCount, unsigned long * SegmCount)
{
	struct kexec_segment * Segments;
	ModuleDescr_t * Mod;
	int segm;

	Segments = malloc(ModCount * sizeof(struct kexec_segment));
	if (Segments == NULL) return NULL;

	Mod = Modules;
	for (segm = 0; segm < ModCount; segm++)
	{
		if ((Mod == NULL) || (Mod->CmdLine == NULL)) {
			free(Segments);
			return NULL;
		}
		Segments[segm].buf = Mod->Module;
		Segments[segm].bufsz = Mod->ModuleSize;
		Segments[segm].mem = (void *)strtol(Mod->CmdLine, NULL, 16);
		SetSegmentMemsz(&Segments[segm]);
	
		printf("raw segment %d:\n  addr: %x\n  size: %x\n", segm + 1, Segments[segm].mem, Segments[segm].bufsz);
	
		Mod = Mod->Next;
	}
	*SegmCount = ModCount;
	return Segments;
}

struct kexec_segment * CreateMultibootKexecSegments(ModuleDescr_t * Modules, int ModCount, unsigned long * SegmCount, void * * Entry)
{
	multiboot_header_t * MbHeader;
	struct kexec_segment * Segments;
	ModuleDescr_t * Mod;
	size_t CmdLineSumSize = 0;
	uint8_t * DescrSegm;
	size_t DescrSegmSize;
	multiboot_info_t * MbInfo;
	multiboot_module_t * MbModule;
	char * CmdLine;
	uint32_t * MbExecCodePtr;
	int SegCnt;
	size_t MemOffs;
	int i, segm;
	
	if (ModCount < 1) return NULL;

	Mod = Modules;
	MbHeader = MultibootFindHeader(Mod->Module, Mod->ModuleSize);
	if (MbHeader == NULL) {
		fprintf(stderr, "unable to locate multiboot header\n");
		return NULL;
	}

	for (i = 0; i < ModCount; i++)
	{
		if (Mod == NULL) return NULL;
		CmdLineSumSize += Mod->CmdLineSize;
		Mod = Mod->Next;
	}
	
	SegCnt = ModCount + 1;
	Segments = malloc(SegCnt * sizeof(struct kexec_segment));
	if (Segments == NULL) return NULL;
	DescrSegmSize = sizeof(multiboot_info_t) + ((ModCount - 1) * sizeof(multiboot_module_t)) + sizeof(MbExecCode_x32) + CmdLineSumSize;
	DescrSegm = malloc(DescrSegmSize);
	if (DescrSegm == NULL) {
		free(Segments);
		return NULL;
	}
	memset(Segments, 0, SegCnt * sizeof(struct kexec_segment));
	memset(DescrSegm, 0, DescrSegmSize);
	
	segm = 0;
	MbInfo = (multiboot_info_t *)DescrSegm;
	MbModule = (multiboot_module_t *)(MbInfo + 1);
	CmdLine = (char *)(MbModule + (ModCount - 1));
	
	//init module segments
	MemOffs = MbHeader->load_addr;
	Mod = Modules;
	for (segm = 0; segm < ModCount; segm++)
	{
		Segments[segm].buf = Mod->Module;
		Segments[segm].bufsz = Mod->ModuleSize;
		Segments[segm].mem = (void *)MemOffs;
		SetSegmentMemsz(&Segments[segm]);
		MemOffs += Segments[segm].memsz;
		Mod = Mod->Next;
	}

	printf("multiboot:\n  addr: %x\n  size: %x\n  entry: %x\n", MbHeader->load_addr, Segments[0].bufsz, MbHeader->entry_addr);

	//init descriptor segment
	Segments[segm].buf = DescrSegm;
	Segments[segm].bufsz = DescrSegmSize;
	Segments[segm].mem = (void *)MemOffs;
	SetSegmentMemsz(&Segments[segm]);

	Mod = Modules;

	if (Mod->CmdLine != NULL) {
		printf("  cmd: [%s]\n", Mod->CmdLine);
		memcpy(CmdLine, Mod->CmdLine, Mod->CmdLineSize);
		MbInfo->flags |= MULTIBOOT_INFO_CMDLINE;
		MbInfo->cmdline = MemOffs + (size_t)CmdLine - (size_t)MbInfo;
		CmdLine += Mod->CmdLineSize;
	}

	if (ModCount > 1) {
		MbInfo->flags |= MULTIBOOT_INFO_MODS;
		MbInfo->mods_count = ModCount - 1;
		MbInfo->mods_addr = MemOffs + (size_t)MbModule - (size_t)MbInfo;

		Mod = Modules->Next;
	
		for (i = 0; i < (ModCount - 1); i++) {
			MbModule[i].mod_start = (size_t)Segments[i + 1].mem;
			MbModule[i].mod_end = MbModule[i].mod_start + Mod->ModuleSize;
			MbModule[i].pad = 0;
			printf("  module %d:\n    addr: %x\n    size: %x\n", i + 1, MbModule[i].mod_start, Mod->ModuleSize);
			if (Mod->CmdLine != NULL) {
				printf("    cmd: [%s]\n", Mod->CmdLine);
				memcpy(CmdLine, Mod->CmdLine, Mod->CmdLineSize);
				MbModule[i].cmdline = MemOffs + (size_t)CmdLine - (size_t)MbInfo;
				CmdLine += Mod->CmdLineSize;
			}
			Mod = Mod->Next;
		}
	}

	MbExecCodePtr = (uint32_t *)CmdLine;
	memcpy(MbExecCodePtr, MbExecCode_x32, sizeof(MbExecCode_x32));
	MbExecCodePtr[MbExec_Exec] = MbHeader->entry_addr;
	MbExecCodePtr[MbExec_Info] = MemOffs;

	*SegmCount = SegCnt;
	*Entry = (void *)(MemOffs + (size_t)(MbExecCodePtr + MbExec_Entry) - (size_t)MbInfo);
	return Segments;
}

int main(int argc, char *argv[])
{
	int ModuleCount;
	ModuleDescr_t * Modules = NULL;
	struct kexec_segment * Segments = NULL;
	unsigned long SegmCount;
	void * Entry;
	unsigned long Flags;
	int DoReboot = 0;
	
	if (argc > 1) {
		if (strcmp(argv[1], "raw") == 0) {
			if (argc > 4) {
				Entry =  (void *)strtol(argv[2], NULL, 16);
				ModuleCount = (argc - 3) / 2;
				Modules = LoadModules(ModuleCount, argv + 3);
				if (Modules != NULL) {
					Segments = CreateRawKexecSegments(Modules, ModuleCount, &SegmCount);
					if (Segments != NULL) {
						printf("entry: %x\n", (uint32_t)Entry);
						Flags = KEXEC_ARCH_386;
					} else {
						fprintf(stderr, "failed to create kexec segments\n");
					}
				} else {
					fprintf(stderr, "failed to load modules\n");
				}
			} else {
				printf("rawexec raw entry module1 addr1 module2 addr2 ...\n");
			}
		} else if (strcmp(argv[1], "mboot") == 0) {
			if (argc > 3) {
				ModuleCount = (argc - 2) / 2;
				Modules = LoadModules(ModuleCount, argv + 2);
				if (Modules != NULL) {
					Segments = CreateMultibootKexecSegments(Modules, ModuleCount, &SegmCount, &Entry);
					if (Segments != NULL) {
						Flags = KEXEC_ARCH_386;
					} else {
						fprintf(stderr, "failed to create kexec segments\n");
					}
				} else {
					fprintf(stderr, "failed to load modules\n");
				}
			} else {
				printf("rawexec mboot main_module cmdline module1_file module1_cmd ...\n");
			}
		} else fprintf(stderr, "unknown mode %s\n", argv[1]);

		if (Segments != NULL) {
			//there is a bug with syscall() return value in some old android libc builds, so use alternative errorcheck
			errno = 0;
			kexec_load(Entry, SegmCount, Segments, Flags);
			if (errno == 0) {
				DoReboot = 1;
			} else {
				fprintf(stderr, "kexec_load failed: %s\n", strerror(errno)); 
			}
		}
	} else {
		printf("rawexec: boot raw and multiboot modules via Linux kexec API\n");
		printf("usage: rawexec raw|mboot ...\n");
	}

	if (Segments) free(Segments);
	if (Modules) FreeModules(Modules);

	if (DoReboot) {
		errno = 0;
		kexec_reboot();
		if (errno != 0) {
			fprintf(stderr, "kexec_reboot failed: %s\n", strerror(errno)); 
		}
	}
	return 0;
}
