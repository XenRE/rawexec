# RawExec

Boot raw and multiboot modules via Linux kexec API. Useful for booting non-Linux OS from existing Android or Linux.
